This is a simple web page built off of PHP, mySQL, and Apache.  
In order to have this program function as developed, you need to have:

1) A working http server
2) A working MySQL server
 a) a database called 'PointSystem'
 b) a table called 'Students'
  i) a row called 'SID'
  ii) a row called 'Name'
  iii) a row called 'MR'
  iv) a row called 'Points'
  v) a row called 'ID'
  vi) a row called 'ServerName'
  vii) a row called 'ID' that autoincrements
 b) A table called 'Users'
  i) a row called 'Username'
  ii) a row called 'Passwd' (encrypted using bcrypt)
  iii) a row called 'ID' that auto increments
3) PHP 5 or higher

The `Program` works like this:
 1) A QR Code is created that points to <theserver>/view.php?<studentid>
 2) Once the QR code is scanned, a page will pop up with a couple of buttons,
  the scanner can then choose whether they want to add or deduct points.

TODO's:
  1) Better Authentication
  2) Make view.php even prettier
  3) Probably disable the view all capability of index.php
  4) General styling for mobile (should be fixed by JS)
  5) Quick install script for windows/linux