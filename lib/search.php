<?php
 include('sql.php');
 require('header.php');
 $sqlquery = new doSQL();
 $search = htmlspecialchars($_POST['search']);
 $sqlquery->doSQLStuff("SELECT * FROM `Students`  WHERE Name Like '%$search%' ORDER BY `Points` DESC, `MR` ASC Limit 10");
 $NAME = $sqlquery->get_student_names();
 $Points = $sqlquery->get_points();
 $ID = $sqlquery->get_id();
 $MR = $sqlquery->get_mr();
?>
<!--Double tabbed for the unseen html and body elements-->
		<div class="w3-quarter w3-container">
			&nbsp;
		</div>
		<div class="w3-half w3-container">
			<br>
			<h3>Results | <a href="/" >Back</a></h3>
			<table class="w3-table-all">
				<tr class="w3-blue">
					<th>Name</th>
					<th>Points</th>
					<th>Mustang Room</th>
				</tr>
				<?php
				for($counter = 0; $counter < sizeof($ID); $counter++){
					// This creates the table rows for the students and their points... kind of like a view all page.
					echo "<tr><td>".strtoupper($NAME[$counter])."</td><td>".$Points[$counter]."</td><td>".$MR[$counter]."</td></tr>";
				} 
				?>
			</table>
		</div>

<?php include('lib/footer.php'); ?>


