<?php
/* This class was created by Gavin Pease for the sole purpose of making
 SQL and PHP talk to eachother a little bit easier. */
class doSQL
{
   /* Create array for student ID (Used for QR code) */
    var $sids = array();

   /* Create array of all points */
    var $points = array();

   /* Create array of all student names */
    var $student_names = array();

   /* Create array of all database ID's */
    var $id = array();

   /* Create array of all mustang rooms */
	var $mustangroom = array();

   /* Create array of all button options */
    var $buttons = array();
   /* set_mr takes in a single array parameter, and sets the
     mustangroom variable */

	function set_mr($a){
		$this->mustangroom = $a;
	}

    /* set_button_array takes in array of buttons, and sets buttons to value */
     function set_button_array($a){
         $this->buttons = $a;
     }
	/* set_sid takes in array of student ids, and sets sids to the value. */

    function set_sid($a){
        $this->sids = $a;
    }

	/* set_id takes in array of database ids (used for editing on the SQL backend, or for indexing),
	  and sets id to the value. */

    function set_id($d){
        $this->id = $d;
    }

	/* set_student_name takes in an array of student names (used for the search function, and displaying name with points),
	  and sets student_names to the value */

    function set_student_name($b){
        $this->student_names = $b;
    }

	/* set_points takes in an array of points, and sets the points variable to this value. */

    function set_points($c){
        $this->points = $c;
    }

	/* get_id returns the arrays of ids sat by set_id() */

    function get_id(){
        return $this->id;
    }
    /* get_buttons returns the array of button options sat by set_button_array() */
     function get_buttons(){
         return $this->buttons;
     }
	/* get_points returns the array of points sat by set_points() */

    function get_points(){
        return $this->points;
    }

	/* get_mr returns the array of mustang room teachers sat by set_mr() */

	function get_mr(){
		return $this->mustangroom;
	}

	/* get_sid returns the array of student ids sat by set_sid() */

    function get_sid(){
        return $this->sids;
    }

	/* get_student_names returns an array of student names sat by set_student_names */

    function get_student_names(){
        return $this->student_names;
    }

	/* doSQLStuff takes in a query string, and performs that action on the database.
	  It can do one of two things:
	  	- If it is a query such as an UPDATE or similar, the method will perform the action,
	  	- If it is a query such as a SELECT, it will populate the variables defined from the
	  		functions above.
	  Be sure to change the variables $servername, $username, $password, and $dbname to whatever
	   is required for your use.
	*/

    function doSQLStuff($query){
        $servername = "localhost";
        $username   = "root";
        $password   = getenv('PointTrackerPasswd');
        $dbname     = "PointSystem";
        $conn       = mysqli_connect($servername, $username, $password, $dbname);
        $result     = mysqli_query($conn, $query);

        $sids       = array();
        $points     = array();
        $names      = array();
        $id         = array();
		$mr			= array();
        $button     = array();
	  // This is skipped if the SQLQuery isn't a SELECT
        while ($row = mysqli_fetch_array($result)) {
            // If we are doing anything with students, their SID is required.
            // Therefore, we use it as a boolean.
            if(isset($row['SID'])){
                array_push($sids, $row['SID']);
                array_push($points, $row['Points']);
    			array_push($names, $row['Name']);
                array_push($id, $row['ID']);
    			array_push($mr, $row['MR']);
            }
            // If we aren't doing anything with a point button, then skip.
            if(isset($row['Amount'])){
                array_push($button, $row['Amount']);
            }
        }
		$this->set_mr($mr);
        $this->set_sid($sids);
        $this->set_points($points);
        $this->set_student_name($names);
        $this->set_id($id);
        $this->set_button_array($button);

    }
}
?>
