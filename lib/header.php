<?php
session_start();
$cookiename="servername";
 if(!isset($_SESSION['servername']) || !isset($_COOKIE['servername']) ){
     if($_SESSION['servername'] != $_COOKIE['servername']){
	        header('Location: /login/login.php?url='.$_SERVER['REQUEST_URI']);
     }
     header('Location: /login/login.php?url='.$_SERVER['REQUEST_URI']);
 }
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Westlake STEM Point Tracker</title>
		<link rel="stylesheet" href="/css/w3.css">
		<!--Custom styleset-->
		<link rel="stylesheet" href="/css/style.css">
		<script src="/js/sketch.js"></script>
	</head>
	<body onresize="checkSize()">
