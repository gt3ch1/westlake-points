<?php

 include('lib/header.php');
 include('lib/sql.php');
$server = $_COOKIE['servername'];
// Create the doSQL object
 $sqlquery = new doSQL();
// Create the query
 $sqlquery->doSQLStuff("SELECT * FROM `Students` WHERE `ServerName` = ".$server." ORDER BY `Points` DESC, `MR` ASC Limit 50");
// Get the arrays of all the names, points, database ID, and mustang rooms
 $NAME = $sqlquery->get_student_names();
 $Points = $sqlquery->get_points();
 $ID = $sqlquery->get_id();
 $MR = $sqlquery->get_mr();
?>
<!--Double tabbed for the unseen html and body elements-->
		<div class="w3-container w3-padding-16">
			<div class="w3-quarter w3-container w3-mobile">
				&nbsp;
			</div>
			<div class="w3-half w3-container w3-mobile" id="content">
				<br>
				<h3>Search for your points!</h3>
				<form action="lib/search.php" method="post">
					<input type="text" name="search" class="search">
					<input type="submit" name="submit" value="Search" class="w3-button w3-black enterbutton w3-mobile">
				</form>
				<br>
				<!-- This will display the top 50 point earners in the database. -->
				<h3>Top 50 Point Earners</h3>
				<table class="w3-table-all w3-mobile" id="table">
					<tr class="w3-blue">
						<th>Name</th>
						<th>Points</th>
						<!-- <th>Mustang Room</th> -->
					</tr>
					<?php
						for($counter = 0; $counter < sizeof($ID); $counter++){
							// This creates the table rows for the students and their points... kind of like a view all page.
							echo "<tr><td>".strtoupper($NAME[$counter])."</td><td>".$Points[$counter]."</td>

                            </tr>";
                            //<td>".$MR[$counter]."</td>
						}
					?>

				</table>
			</div>
		</div>
<?php include ('lib/footer.php'); ?>
<!--TODO: Pretty-fy this -->
