<!--TODO: Add password auth... pull from https://github.com/UndefinedIndustries/SQLSprinkler -->
<?php
	include('lib/header.php');
	include('lib/sql.php');
	$id = $_GET['id'];
	$sqlquery = new doSQL();
    $buttonQuery = new doSQL();
    $sqlquery->doSQLStuff("SELECT * FROM `Students` WHERE `SID` = ".$id." AND
			 ServerName=".$_COOKIE['servername']);
    $buttonQuery->doSQLStuff("SELECT * FROM `Buttons` WHERE         ServerName=".$_COOKIE['servername']);

    $SID = $sqlquery->get_sid();
    $Points = $sqlquery->get_points();
    $ID = $sqlquery->get_id();
	$name = $sqlquery->get_student_names();
    $buttonArray = $buttonQuery->get_buttons();
?>
<!--Double tabbed for the unseen html and body elements-->
		<div class="w3-container margin w3-row">
            <center>
    			<p>Student: <?php echo $name[0]; ?></p>
    			<p>Points: <?php echo $Points[0]; ?></p>
    			<br>
    			<p>Change Points:</p>

    			<form action="lib/changepoints.php" method="post">
    				<div style="width:12.5%!important;" class="w3-col">&nbsp;</div>
    				<div class="w3-threequarter" id="content">
                        <table class="w3-table-all">
                            <?php
                                for($counter = 0; $counter < sizeof($buttonArray); $counter++){
                                    // This creates the table rows for the buttons so that it is easier to change the system on the fly
                            ?>
                            <tr>
                                <td>
                                    <button class="w3-button w3-red w3-half" name="action" value="<?php echo $buttonArray[$counter]*-1; ?>"><?php echo $buttonArray[$counter]*-1; ?></button>
                                </td>
                                <td>
                                    <button class="w3-button w3-blue w3-half" name="action" value="<?php echo $buttonArray[$counter]; ?>">+<?php echo $buttonArray[$counter]; ?></button>
                                </td>
                            </tr>
                            <?php
                                }
                            ?>
                        </table>

    					<!--TODO: Store mode.		-->
    					<input type="hidden" value="<?php echo $ID[0]; ?>" name="id"/>
    				</div>
    			</form>
            </center>
		</div>
<?php include('lib/footer.php') ?>
